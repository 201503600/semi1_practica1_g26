import React from 'react';
import Grid from '@material-ui/core/Grid';
import Dialogo from './Dialogo';
import Avatar from '@material-ui/core/Avatar';
import Divider from '@material-ui/core/Divider';
import Album from './Album';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import ruta from './Ruta';
import '../Styles/VerFotos.css';

class VerFotos extends React.Component{
    titulo = '';
    mensaje = '';
    state = {
      userName: '',
      photo: '',
      photos: [],
      photosProfile: [],
      openConfirmacion: false
    }

    abrirConfirmacion = (titulo, mensaje) => {
      this.titulo = titulo;
      this.mensaje = mensaje;
      this.setState({
        openConfirmacion: true
      })
    }

    cerrarConfirmacion = () => {
      this.setState({
        openConfirmacion: false
      })
    }
  
    goInicio(){
      window.location.href = `/Inicio`;
    }
  async componentDidMount() {
    await this.fetchConsulta();
    this.setState({
      userName: localStorage.getItem('user')
    });
  }

  fetchConsulta = async () => {
    try{
        let req = await fetch('http://' + ruta.ip + ':' + ruta.port + '/ver-fotos?idUser=' + localStorage.getItem('idUser'));
        let data = await req.json();
        if (data['status'] == 200){
          this.setState({
            photos: data['photos'],
            photosProfile: data['photosProfile'],
            photo: 'https://practica1-g26-imagenes.s3.us-east-2.amazonaws.com/' + data['photo']
          })
        }else if (data['status'] == 400){
          this.abrirConfirmacion('ERROR','Credenciales incorrectas!');
        }else{
          this.abrirConfirmacion('ERROR','Ocurrio un error! Intente de nuevo.');
        }
    } catch (error) {
      console.log(error);
      this.abrirConfirmacion('ERROR','Ocurrio un error! Intente de nuevo.');
    }
  }

  render() {
      return (
        <div item xs={12} sm={12} className="root">
          <h1>VER FOTOS</h1>
          <Divider></Divider>
          <Grid container spacing={2}style={{paddingTop: "2em"}}>
            <Grid item xs={12} sm={4} style={{paddingLeft: "3em"}}>

              <Grid container spacing={2} justify="center">
                <Avatar className="img" src={this.state.photo} alt="Travis Howard"/>
              </Grid>
              <Grid container spacing={2} justify="center">
                <h2>{this.state.userName}</h2>
              </Grid>
              <br/>
              <br/>
              <br/>
              <br/>
              <Grid container spacing={2} justify="center">
                <Typography >
                  <Link href="#" onClick={this.goInicio}>
                    <ArrowBackIcon/>
                    Regresar
                  </Link>
                </Typography>
              </Grid>
            </Grid>
            <Grid item xs={12} sm={8} style={{paddingRight: "3em"}}>
              
                <Album key={"profile"} Nombre="Fotos de Perfil" Photos={this.state.photosProfile} />
                  {
                    /*this.state.photos.map((item) => {
                      return(
                        <Album key={item.album} Nombre={item.album} Photos={item.photos}/>
                      )
                    })*/
                    this.state.photos.map((item) => {
                      console.log(item)
                      return(
                        <Album key={item[0]} Nombre={item[0]} Photos={item[1]}/>
                      )
                    })
                  }    
                
            </Grid>
          </Grid>
          
          <Dialogo 
              titulo = {this.titulo}
              mensaje = {this.mensaje}
              open = {this.state.openConfirmacion}
              cerrar = {this.cerrarConfirmacion}
            />
        </div>
      );
    }
}

export default VerFotos;