import React from 'react';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import Avatar from '@material-ui/core/Avatar';
import Chip from '@material-ui/core/Chip';
import Dialogo from './Dialogo';
import Chat from './Chat';
import FaceIcon from '@material-ui/icons/Face';
import DoneIcon from '@material-ui/icons/Done';

import '../Styles/Inicio.css';
import ruta from './Ruta';

export default class Inicio extends React.Component{
  state = {
    user:'',
    nombre:'',
    imagen:'',
    etiquetas: [],
    openError: false
  }

  async componentDidMount() {
    await this.fetchConsulta();
  }

  fetchConsulta = async () => {
    try{
        let req = await fetch('http://' + ruta.ip + ':' + ruta.port + '/inicio?idUser=' + localStorage.getItem('idUser'));
        let data = await req.json();
        if (data['status'] == 200){
          this.setState({
            user: data['user'],
            nombre: data['name'],
            etiquetas: data['labels'],
            imagen: `https://practica1-g26-imagenes.s3.us-east-2.amazonaws.com/${data['photo']}`
          });
          localStorage.setItem('user', data['user']);
          console.log(this.state.etiquetas);
        }else if (data['status'] == 400){
          this.abrirError('Credenciales incorrectas!');
        }else{
          this.abrirError('Ocurrio un error! Intente de nuevo');
        }
    } catch (error) {
      this.abrirError('Ocurrio un error! Intente de nuevo');
    }
  }

  goInicio(){
    localStorage.clear();
    window.location.href = `/Login`;
  }

  abrirError = (mensaje) => {
    this.titulo = 'ERROR';
    this.mensaje = mensaje;
    this.setState({
      openError: true
    })
  }

  cerrarError = () => {
    this.setState({
      openError: false
    })
  }

  handleEditarFoto () {
    window.location.href = `/EditarPerfil`;
  }

  handleVerFotos(){
    window.location.href = `/VerFotos`;
  }

  handleSubirFoto(){
    window.location.href = `/SubirFoto`;
  }

  handleExtraerTexto(){
    window.location.href = `/ExtraerTexto`;
  }

  render() {
    return (
      <>
        <Chat/>
      <div className="root">
        
        <Paper className="paper" elevation={5}>
          <Grid container spacing={2} direction="column">
                <Grid item xs={12} xs>
                  <h1>
                    DATOS PERSONALES
                  </h1>
                  <Divider/>
                </Grid>
                <br/>
                <br/>
                <Grid container className="body" spacing={2} direction="row">
                <Grid item xs={4} >
                    <div style={{textAlign:'center'}}>
                      <Avatar className="img" src={this.state.imagen}
                        alt="Travis Howard"
                      />
                      </div>
                      <br/>
                      <div style={{textAlign:'center'}}>
                      {
                        this.state.etiquetas.map((item) => {
                          return(
                              <Chip
                                size="small"
                                icon={<FaceIcon />}
                                label={item}
                                key={item}
                                color="primary"
                                deleteIcon={<DoneIcon />}
                                style={{margin:2}}
                              />
                          )
                        })
                      }
                      </div>
                </Grid>
                <Grid item xs={5} >    
                    <Typography gutterBottom variant="overline">Nombre de Usuario</Typography>
                    <Divider></Divider>
                    <TextField id="outlined-basic1" label={this.state.user} variant="outlined" fullWidth disabled={true}/>    
                    <Typography gutterBottom variant="overline">Nombre Completo</Typography>
                    <Divider></Divider>
                    <TextField id="outlined-basic" label={this.state.nombre} variant="outlined" fullWidth disabled={true}/>
                </Grid>            
                <Grid item xs={3} style={{paddingTop:"center"}}>                
                  <Box display="flex" flexDirection="row-reverse" p={1} m={1} bgcolor="background.paper">    
                    <Button variant="outlined" color="primary" onClick={this.handleEditarFoto}>
                      Editar Perfil
                    </Button>
                  </Box>
                  <Box display="flex" flexDirection="row-reverse" p={1} m={1} bgcolor="background.paper">    
                    <Button variant="outlined" color="primary" onClick={this.goInicio}>
                      Cerrar Sesion
                    </Button>
                  </Box>
                </Grid>
              </Grid>
          </Grid>
          <br/>
          <br/>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Grid container justify="center" spacing={10}>
                <Grid item>
                  <Button variant="outlined" color="primary" onClick={this.handleVerFotos}>
                    Ver Fotos
                  </Button>
                </Grid>
                <Grid item>
                  <Button variant="outlined" color="primary" onClick={this.handleSubirFoto}>
                    Subir Foto
                  </Button>
                </Grid>            
                <Grid item>
                  <Button variant="outlined" color="primary" onClick={this.handleExtraerTexto}>
                    Extraer Texto
                  </Button>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Paper>
        
        <Dialogo 
          titulo = {this.titulo}
          mensaje = {this.mensaje}
          open = {this.state.openError}
          cerrar = {this.cerrarError}
        />
      </div>
      </>
    );
  }
  }
