import React from 'react';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import Dialogo from './Dialogo';
import ReconocimientoFacial from './ReconocimientoFacial';

import md5 from 'md5';
import '../Styles/Login.css';
import ruta from './Ruta';


class Login extends React.Component{
    titulo = '';
    mensaje = '';
    state = {
      email: '',
      password: '',
      openError: false,
      openReconocimiento: false,
      camera: '',
    }

    goInicio = async() => {
      let config = {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'POST, GET',
            'Access-Control-Request-Method': '*',
            'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
        },
        body: JSON.stringify({
            user: this.state.email,
            pass: md5(this.state.password)
        }),
        mode: 'cors'
      }
      //console.log(config.body.photo);
      try {
        let response = await fetch('http://' + ruta.ip + ':' + ruta.port + '/login', config)
        let data = await response.json();
        console.log(data);
        if (data['status'] == 200){
          localStorage.setItem('idUser',data['idUser']);
          localStorage.setItem('user',data['user']);
          window.location.href = `/Inicio`;
        }else if (data['status'] == 400){
          this.abrirError('Credenciales incorrectas!');
        }else{
          this.abrirError('Ocurrio un error! Intente de nuevo');
        }        
      } catch (error) {
        this.abrirError('Ocurrio un error! Intente de nuevo');
      }
    }

    abrirError = (mensaje) => {
      this.titulo = 'ERROR';
      this.mensaje = mensaje;
      this.setState({
        openError: true
      })
    }

    cerrarError = () => {
      this.setState({
        openError: false
      })
    }

    loginWithCamera = async(imagen) => {
      this.state.camera = imagen; 
      let config = {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'POST, GET',
            'Access-Control-Request-Method': '*',
            'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
        },
        body: JSON.stringify({
            user: this.state.email,
            camera: this.state.camera.replace(/^data:image\/\w+;base64,/, "")
        }),
        mode: 'cors'
      }
      //console.log(config.body.photo);
      try {
        let response = await fetch('http://' + ruta.ip + ':' + ruta.port + '/loginCamera', config)
        let data = await response.json();
        console.log(data);
        if (data['status'] == 200){
          localStorage.setItem('idUser',data['idUser']);
          localStorage.setItem('user',data['user']);
          window.location.href = `/Inicio`;
        }else if (data['status'] == 400){
          this.abrirError('Credenciales incorrectas!');
        }else{
          this.abrirError('Ocurrio un error! Intente de nuevo');
        }        
      } catch (error) {
        this.abrirError('Ocurrio un error! Intente de nuevo');
      }
    }

    setEmail = async e  => {
      this.state.email = e.target.value;
    }

    setPass = async e  => {
      this.state.password = e.target.value;
    }

    goRegistrar(){
      window.location.href = `/Registro`;
    }

    handleReconocimiento = () => {
      this.setState({
        openReconocimiento: true
      })
    }

    cerrarReconocimiento = () => {
      this.setState({
        openReconocimiento: false
      })
    }

    render() {
        return (
          <>
            <Grid container className="root" spacing={2} justify="center">
              <Paper className="paper" elevation={5} justify="center">
                <div style={{width: "100%", display: "flex"}}>
                  <div style={{width: "30%", paddingTop: "2em"}}>
                        <Avatar className="img" src=""
                          alt="Travis Howard"
                        />
                  </div>

                  <div style={{width: "80%", paddingLeft: "2em"}}>
                    <br/>
                    <TextField
                      label="UserName"
                      fullWidth
                      margin="normal"
                      onChange={this.setEmail}
                    />
                    <TextField
                      label="Password"
                      type="password"
                      fullWidth
                      margin="normal"
                      onChange={this.setPass}
                    />
                  </div>
                </div>

                <Grid container  spacing={2} justify="center" style={{marginLeft: "6em"}}>
                  <Button variant="outlined" color="primary" onClick={this.goInicio}>
                    log in
                  </Button>

                  <Button variant="outlined" color="primary" onClick={this.handleReconocimiento} style={{marginLeft: "3em"}}>
                    Entrar por camara
                  </Button>
                </Grid>
                <br/>
                <br/>
                <br/>
                <Grid container spacing={2} justify="center">
                  <Typography >
                    <Link href="#" onClick={this.goRegistrar}>
                      No tienes cuenta? Registrate!
                    </Link>
                  </Typography>
                </Grid>              
              </Paper>
              <Dialogo 
                titulo = {this.titulo}
                mensaje = {this.mensaje}
                open = {this.state.openError}
                cerrar = {this.cerrarError}
              />
              <ReconocimientoFacial
              open={this.state.openReconocimiento}
              cerrarReconocimiento = {this.cerrarReconocimiento}
              setCamera={this.loginWithCamera}
              cerrarReconocimiento={this.cerrarReconocimiento}
              />
            </Grid>
          </>
        );
    }
}

export default Login;