import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import Webcam from 'react-webcam';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const videoConstraints = {
    width: 400,
    height: 420
};

export default function DialogoTomarFoto(props) {
  const webcamRef = React.useRef(null);

  const handleCapture = React.useCallback(
    async() => {
      let now= new Date();
      const imageSrc = webcamRef.current.getScreenshot();
      props.handleCambiarFoto(`${now.getTime()}.jpg`,imageSrc);
      props.cerrarDialogo();
    }
  );

  return (
    <div>
      <Dialog
        open={props.open}
        TransitionComponent={Transition}
        keepMounted={props.open}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle id="alert-dialog-slide-title">{"Tomar foto"}</DialogTitle>
        <DialogContent>
            <Webcam
                audio={false}
                ref={webcamRef}
                screenshotFormat="image/jpeg"
                videoConstraints={videoConstraints}

            />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCapture} color="primary">
            CAPTURAR
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
