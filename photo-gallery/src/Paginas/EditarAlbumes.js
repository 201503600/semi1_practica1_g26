import React from 'react';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Input from '@material-ui/core/Input';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import Dialogo from './Dialogo';

import '../Styles/EditarAlbumes.css';
import ruta from './Ruta';

export default class EditarAlbumes extends React.Component{
  state = {
    albumCrear: '',
    album: '',
    albumes: [],
    openDialogo: false
  }

  handleChange = (event) => {
    this.setState({album: event.target.value});
  }

  handleChangeCrear = (event) => {
    this.setState({albumCrear: event.target.value});
  }

  async componentDidMount() {
    await this.fetchConsulta();
  }

  fetchConsulta = async () => {
    try{
        let req = await fetch('http://' + ruta.ip + ':' + ruta.port + '/editar-albumes?idUser=' + localStorage.getItem('idUser'));
        let data = await req.json();
        if (data['status'] == 200){
          this.setState({
            albumes: data['albumes']
          })
        }else if (data['status'] == 400){
          this.abrirDialogo('ERROR', 'Credenciales incorrectas!')
        }else{
          this.abrirDialogo('ERROR', 'Ocurrio un error! Intente de nuevo')
        }
    } catch (error) {
      this.abrirDialogo('ERROR', 'Ocurrio un error! Intente de nuevo')
    }
  }

  handleCrear = async() => {
    if (this.state.albumCrear != ''){
      let config = {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'POST, GET',
            'Access-Control-Request-Method': '*',
            'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
        },
        body: JSON.stringify({
            idUser: localStorage.getItem('idUser'),
            operation: 'crear',
            album: this.state.albumCrear
        }),
        mode: 'cors'
      }
      //console.log(config.body.photo);
      try {
        let response = await fetch('http://' + ruta.ip + ':' + ruta.port + '/editar-albumes', config)
        let data = await response.json();
        console.log(1,data);
        if (data['status'] == 200){
          this.abrirDialogo('CREAR', 'Se creo el album correctamente!')
        }else{
          this.abrirDialogo('ERROR', 'Ocurrio un error! Intente de nuevo')
        }
        await this.fetchConsulta();        
      } catch (error) {
        this.abrirDialogo('ERROR', 'Ocurrio un error! Intente de nuevo')
      }
    }    
    //console.log(await res.json());
  }

  handleEliminar = async() => {
    if (this.state.album != ('' || 'Ninguno')){
      let config = {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'POST, GET',
            'Access-Control-Request-Method': '*',
            'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
        },
        body: JSON.stringify({
            idUser: localStorage.getItem('idUser'),
            operation: 'eliminar',
            album: this.state.album
        }),
        mode: 'cors'
      }
      //console.log(config.body.photo);
      try {
        let response = await fetch('http://' + ruta.ip + ':' + ruta.port + '/editar-albumes', config)
        let data = await response.json();
        console.log(data);
        if (data['status'] == 200){
          this.abrirDialogo('ELIMINAR', 'Se eliminó correctamente el album!')
        }else{
          this.abrirDialogo('ERROR', 'Ocurrio un error! Intente de nuevo')
        }
        this.setState({
          album: 'Ninguno'
        })
        await this.fetchConsulta();
      } catch (error) {
        this.abrirDialogo('ERROR', 'Ocurrio un error! Intente de nuevo')
      }
      
    }
  }

  abrirDialogo = (titulo, mensaje) => {
    this.titulo = titulo;
    this.mensaje = mensaje;
    this.setState({
      openDialogo: true
    })
  }

  cerrarDialogo = () => {
    this.setState({
      openDialogo: false
    })
  }

  goInicio(){
    window.location.href = `/Inicio`;
  }

  render(){
    return (
      <div className="root">
        <Paper className="paper" elevation={5}>
          <Grid container spacing={2}>
            <Grid item xs={12} xs container>
              <Grid item xs container direction="column" spacing={2}>
                <Grid item xs>
                  <h1>
                    EDITAR ALBUMES
                  </h1>
                  <Divider/>
                </Grid>
                <Grid item>
                  <Typography variant="body2" gutterBottom>
                    Nombre de Album
                  </Typography>
                  <Input placeholder="" value={this.state.albumCrear} onChange={this.handleChangeCrear} fullWidth inputProps={{ 'aria-label': 'description' }} />
                </Grid>
                <Grid item> 
                  <Box display="flex" flexDirection="row-reverse" p={1} m={1} bgcolor="background.paper">    
                    <Button variant="outlined" color="primary" onClick={this.handleCrear}>
                      Crear
                    </Button>
                  </Box>
                </Grid>
                <Divider/>    
                <Divider/>    
                <Divider/>    
                <Divider/>  
                <Divider/>    
                <Divider/>              
                <Grid item>
                  <Typography variant="body2" gutterBottom>
                    Selecciona Album
                  </Typography>
                  <FormControl variant="outlined" className="formControl" fullWidth>
                    <InputLabel id="demo-simple-select-outlined-label">Album</InputLabel>
                    <Select
                      labelId="demo-simple-select-outlined-label"
                      id="demo-simple-select-outlined"
                      value={this.state.album}
                      onChange={this.handleChange}
                      label="Album"
                      fullWidth
                    >
                      <MenuItem key="Ninguno" value="Ninguno">
                      <em>None</em>
                      </MenuItem>
                      {
                        this.state.albumes.map((item) => {
                          return(
                            <MenuItem key={item.album} value={item.album}>
                              <em>
                                {item.album}
                              </em>
                            </MenuItem>
                          )
                        })
                      }
                    </Select>
                  </FormControl>
                </Grid>
                <Grid item> 
                  <Box display="flex" flexDirection="row-reverse" p={1} m={1} bgcolor="background.paper">    
                    <Button variant="outlined" color="primary" 
                      onClick={() => { if (window.confirm('Esta seguro de eliminar el album?')) this.handleEliminar() }}>
                      Eliminar
                    </Button>
                  </Box>
                </Grid>
                <Typography >
                  <Link href="#" onClick={this.goInicio}>
                    <ArrowBackIcon/>
                    Regresar
                  </Link>
                </Typography>
              </Grid>
            </Grid>
          </Grid>
        </Paper>
        <Dialogo 
          titulo = {this.titulo}
          mensaje = {this.mensaje}
          open = {this.state.openDialogo}
          cerrar = {this.cerrarDialogo}
        />
      </div>
    );
  }
}