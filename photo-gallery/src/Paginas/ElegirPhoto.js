import React from 'react';

class ElegirPhoto extends React.Component {
    constructor(props){
        super(props);
    }

    getBase64 = file => {
        return new Promise(resolve => {
          let fileInfo;
          let baseURL = "";
          // Make new FileReader
          let reader = new FileReader();
          // Convert the file to base64 text
          reader.readAsDataURL(file);    
          // on reader load somthing...
          reader.onload = () => {
            // Make a fileInfo Object
            baseURL = reader.result;
            //this.setState({imagen: baseURL});
            resolve(baseURL);
          };
        });
    };

    handleFileInputChange = e => {
        let file = e.target.files[0];
    
        this.getBase64(file)
        .then(result => {
            file["base64"] = result;
            //console.log(result);
            //console.log(result.replace(/^data:image\/\w+;base64,/, ""));
            this.props.handleCambiarFoto(file['name'], result)
        })
        .catch(err => {
            console.log(err);
        });
    
        this.setState({
          file: e.target.files[0]
        });
    };

    render(){
        return (
            <>
                <input type="file" id="fichero" onChange={this.handleFileInputChange} />
                <label htmlFor="fichero" className="subirFoto">SELECCIONAR FOTO</label>
            </>
        );
    }
}

export default ElegirPhoto;