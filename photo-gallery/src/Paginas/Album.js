import React from 'react';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia';
import Avatar from '@material-ui/core/Avatar';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Foto from './Foto';
import '../Styles/Album.css';

class Album extends React.Component {
    constructor(props){
        super(props);
    }

    render() {
      return (
        <>
            <fieldset className="fieldset">
                <legend>{this.props.Nombre}</legend>
                <Grid item xs={12} style={{width:"100%", padding:"1em"}}>
                    <Paper className="paper" elevation={5} id="growth" style={{height:"300px"}}>
                        <div >
                            {
                                this.props.Photos.map((item, key) =>{
                                    let image = '';
                                    if (item.foto != undefined)
                                        image = 'http://practica1-g26-imagenes.s3.us-east-2.amazonaws.com/' + item.foto;
                                    else
                                        image = 'http://practica1-g26-imagenes.s3.us-east-2.amazonaws.com/' + item.S;
                                    
                                    return(
                                        <Foto
                                            key={key}
                                            image={image}
                                            item={item}
                                        />
                                    )
                                })
                            }
                        </div>
                    </Paper>
                </Grid>
            </fieldset>
            
            <br/>
        </>
        );
    }
  }

  export default Album;