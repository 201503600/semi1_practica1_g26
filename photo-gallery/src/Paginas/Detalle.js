import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';

import Avatar from '@material-ui/core/Avatar';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';

import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import Dialogo from './Dialogo';
import ruta from './Ruta';

class Detalle extends React.Component{
    constructor(props) {
        super(props);
        console.log(props)
        this.state = {
            Descripcion: this.props.Descripcion,
            Idioma: '',
            openDialogo: false
        }
        this.titulo = '';
        this.mensaje = '';
    }

    ElegirIdioma = async (e) => {
        await this.setState({
            Idioma: e.target.value
        })
    }
    
    handleTraducir = async() => {
        try {
            let config = {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Methods': 'POST, GET',
                    'Access-Control-Request-Method': '*',
                    'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
                },
                body: JSON.stringify({
                    texto: this.state.Descripcion,
                    idioma: this.state.Idioma
                }),
                mode: 'cors'
            }
            
            let res = await fetch('http://' + ruta.ip + ':' + ruta.port + '/traducir', config)
            let req = await res.json();
    
            if (req['status'] == 200){
                this.setState({
                    Descripcion: req['data']['TranslatedText']
                });
            }else{
                this.abrirDialogo('ERROR', 'Ocurrio un error! Intente de nuevo')    
            }
        } catch (error) {
            this.abrirDialogo('ERROR', 'Ocurrio un error! Intente de nuevo')
        }
    }
    
    abrirDialogo = (titulo, mensaje) => {
        this.titulo = titulo;
        this.mensaje = mensaje;
        this.setState({
            openDialogo: true
        })
    }

    cerrarDialogo = () => {
        this.setState({
            openDialogo: false
        })
    }

    handleClose = () => {
        this.props.cerrar();
    }

    render() {
        let {Nombre, Foto, Descripcion, open} = this.props;
        
        return (
            <Dialog
                open = {open}
                onClose={this.handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-de2scription"
                style={{minWidth: "30em"}}
            >
                <DialogTitle id="alert-dialog-title">{Nombre}</DialogTitle>
                <DialogContent>
                    <div style={{minWidth: "100%", textAlign: "center"}}>
                        
                        <Avatar
                            src={Foto} alt="Foto"
                            style={{
                                marginTop: "auto",
                                marginBottom: "auto",
                                marginLeft: "4em",
                                marginRight: "4em",
                                
                                minWidth: "12em",
                                minHeight: "12em"
                            }}
                            variant="square"
                        />
                        <br/>
                        <DialogContentText id="alert-dialog-description">
                            {this.state.Descripcion}
                        </DialogContentText>
                    </div>

                    <div style={{minWidth: "100%"}}>
                        <div style={{maxWidth: "50%", float: "left", textAlign: "center", margin: "1em"}}>
                            <Select
                                style={{width: "8em"}}
                                value={this.state.Idioma}
                                onChange={this.ElegirIdioma}
                                displayEmpty
                            >
                                <MenuItem value=""><em>None</em></MenuItem>
                                <MenuItem value={'Inglés'}>Inglés</MenuItem>
                                <MenuItem value={'Portugués'}>Portugués</MenuItem>
                                <MenuItem value={'Francés'}>Francés</MenuItem>
                                <MenuItem value={'Sueco'}>Sueco</MenuItem>
                            </Select>
                        </div>

                        <div style={{maxWidth: "50%", float: "right", textAlign: "center", margin: "1em"}}>
                            <Button 
                                style={{width: "8em"}}
                                variant="outlined"
                                color="primary"
                                onClick={this.handleTraducir}
                                >
                                Traducir
                            </Button>
                        </div>
                    </div>
                </DialogContent>
                
                <DialogActions>
                    <Typography style={{float: "left", minWidth: "100%"}}>
                        <Link href="#" onClick={this.handleClose}>
                            <ArrowBackIcon/>
                            Regresar
                        </Link>
                    </Typography>
                </DialogActions>
                <br/>
                <Dialogo 
                    titulo = {this.titulo}
                    mensaje = {this.mensaje}
                    open = {this.state.openDialogo}
                    cerrar = {this.cerrarDialogo}
                />
            </Dialog>
        );
    }
}

export default Detalle;