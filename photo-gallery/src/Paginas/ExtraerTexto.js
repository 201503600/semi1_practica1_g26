import React from 'react';
import Dialogo from './Dialogo';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Divider from '@material-ui/core/Divider';
import ElegirPhoto from './ElegirPhoto'
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';

import '../Styles/ExtraerTexto.css';
import ruta from './Ruta';

class EditarPerfil extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            Imagen: '',
            Texto: '',
            openDialogo: false
        }
        this.titulo = '';
        this.mensaje = '';
    }
    
    handleCambiarFoto = async (namephoto, photo) => {
        this.setState({
          Imagen: photo
        });

        let config = {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'POST, GET',
                'Access-Control-Request-Method': '*',
                'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
            },
            body: JSON.stringify({
                Imagen: this.state.Imagen
            }),
            mode: 'cors'
          }
          try { 
            let response = await fetch('http://' + ruta.ip + ':' + ruta.port + '/extraer-texto', config)
            let data = await response.json();
            if (data['status'] == 200){
                this.setState({
                    Texto: data['text']
                });
            }else{
                this.abrirDialogo('ERROR', 'Ocurrio un error! Intente de nuevo')
            }
        } catch (error) {
            this.abrirDialogo('ERROR', 'Ocurrio un error! Intente de nuevo')
        }
    }

    abrirDialogo = (titulo, mensaje) => {
        this.titulo = titulo;
        this.mensaje = mensaje;
        this.setState({
            openDialogo: true
        })
    }

    cerrarDialogo = () => {
        this.setState({
            openDialogo: false
        })
    }

    goInicio(){
        window.location.href = `/Inicio`;
    }

    render() {
        return (
            <Grid container className="root" spacing={2}>
                <Paper className="Papel" elevation={5}>
                    <h1 className="Foto">EXTRAER TEXTO</h1>
                    <Typography style={{float: "right", maxWidth: "10em", paddingTop: "2em"}}>
                        <Link href="#" onClick={this.goInicio}>
                            <ArrowBackIcon/>
                            Regresar
                        </Link>
                    </Typography>
                    <Divider/>
                    <br/>
                    <div className="Foto">
                        <ElegirPhoto handleCambiarFoto={this.handleCambiarFoto}/>
                    </div>
                    <br/>
                    <Grid container spacing={3} justify="center">
                        <Grid item xs={6}>
                            <div className="Panel">
                                <img
                                    style={{maxHeight: "32rem"}}
                                    className="ImagenExtraer" src={this.state.Imagen}/>
                            </div>
                        </Grid>
                        <Grid item xs={6}>
                            <div className="Panel">
                                <TextField
                                    fullWidth
                                    id="outlined-multiline-static"
                                    className="Panel"
                                    multiline
                                    rows={25}
                                    variant="outlined"
                                    value={this.state.Texto}
                                />
                            </div>
                        </Grid>
                    </Grid>
                    <br/>
                </Paper>

                <Dialogo 
                    titulo = {this.titulo}
                    mensaje = {this.mensaje}
                    open = {this.state.openDialogo}
                    cerrar = {this.cerrarDialogo}
                />
          </Grid>
        );
    }
}

export default EditarPerfil;