import React, {Component, useRef} from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import Webcam from 'react-webcam';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const videoConstraints = {
    width: 400,
    height: 420
};

export default function ReconocimientoFacial(props) {
    const [foto, setFoto] = React.useState('');
    const webcamRef = React.useRef(null);

    React.useEffect(async() => {
        await setTimeout( async () => {
            handleCapture()
        }, 3000)
    });

    const handleCapture = React.useCallback(
        async() => {
            if(props.open){
                try {
                    const imageSrc = webcamRef.current.getScreenshot();
                    if(false){
                        props.cerrarReconocimiento();
                    }else{
                        setFoto(imageSrc);
                        props.setCamera(imageSrc);
                        props.cerrarReconocimiento();
                    }   
                } catch (error) {
                    
                }
            }
        }
    );

    return (
        <div>
          <Dialog
            open={props.open}
            TransitionComponent={Transition}
            keepMounted={props.open}
            aria-labelledby="alert-dialog-slide-title"
            aria-describedby="alert-dialog-slide-description"
          >
            <DialogTitle id="alert-dialog-slide-title">{"Reconocimiento facial"}</DialogTitle>
            <DialogContent>
                <Webcam
                    audio={false}
                    ref={webcamRef}
                    screenshotFormat="image/jpeg"
                    videoConstraints={videoConstraints}
                />
            </DialogContent>
            <DialogActions>
                <Button onClick={props.cerrarReconocimiento} color="primary">
                    Cancelar
                </Button>
            </DialogActions>
          </Dialog>
        </div>
    );
}