import React from 'react';
import QuestionAnswer from '@material-ui/icons/QuestionAnswer';
import Send from '@material-ui/icons/Send';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';
import Popper from '@material-ui/core/Popper';
import { makeStyles } from '@material-ui/core/styles';

import ruta from './Ruta';

const useStyles = makeStyles((theme) => ({
    paper: {
      marginRight: theme.spacing(2),
      width: "30em",
      height: "26em",
      border: "1px solid",
    },
    mensajes: {
        height: '90%',
        maxheight: '90%',
        overflowY: 'scroll',
        marginRight: theme.spacing(1),
        marginLeft: theme.spacing(1),
    },
    envio: {
        display: 'flex',
        height: '10%',
        marginRight: theme.spacing(1),
        marginLeft: theme.spacing(1),
        alignItems: 'center'
    },
    msgIzquierda: {
        display: 'flex',
        float: 'left',
        width: "96%",
        margin: theme.spacing(1),
    },
    msgDerecha: {
        display: 'block',
        width: "96%",
        margin: theme.spacing(1),
    },
    pIzquierda: {
        float: 'left',
        color: 'white',
        padding: theme.spacing(1),
        backgroundColor: '#4A94B9'
    },
    pDerecha: {
        float: 'right',
        color: 'white',
        padding: theme.spacing(1),
        backgroundColor: 'gray'
    }
}));

export default function Chat() {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const [text, setText] = React.useState('');
    let [listaMensajes, setLista] = React.useState([
            {
                emisor: 'bot',
                mensaje: 'Hola! En que puedo servirte?'
            }
    ]);
    const anchorRef = React.useRef(null);

    const handleToggle = () => {
        setOpen(!open);
    };

    const sendMessage = async() => {
        let lista = listaMensajes.slice();
        lista.push({emisor:'user', mensaje:text});
        //listaMensajes.push({emisor:'user', mensaje:text});
        let config = {
          method: 'POST',
          headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Access-Control-Allow-Origin': '*',
              'Access-Control-Allow-Methods': 'POST, GET',
              'Access-Control-Request-Method': '*',
              'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
          },
          body: JSON.stringify({
              idUser: localStorage.getItem('idUser'),
              texto: text
          }),
          mode: 'cors'
        }
        console.log(listaMensajes);
        try {
          let response = await fetch('http://' + ruta.ip + ':' + ruta.port + '/chatbot', config)
          let data = await response.json();
          console.log(data);
          if (data['status'] == 200){
            console.log(data);
            lista.push({emisor:'bot', mensaje:data['mensaje']});
            console.log(lista);
          }else{
            this.abrirError('Ocurrio un error! Intente de nuevo');
          }        
        } catch (error) {
          this.abrirError('Ocurrio un error! Intente de nuevo');
        }
        setText('');
        setLista(lista);
      }

    const handleChange = (event) => {
        setText(event.target.value);
    };

    return (
        <div>
            <Popper open={open} anchorEl={anchorRef.current} role={undefined} transition>
            {({ TransitionProps, placement }) => (
                <Grow
                    {...TransitionProps}
                    style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center top' }}
                >
                    <Paper className={classes.paper}>
                        <div className={classes.mensajes}>
                            {
                                listaMensajes.map((item, key) =>{
                                    if(item.emisor === 'user'){
                                        return (
                                            <div key={key} className={classes.msgIzquierda} >
                                                <Paper className={classes.pIzquierda}>
                                                    <Typography variant="body1" component="p">
                                                        {item.mensaje}
                                                    </Typography>
                                                </Paper>
                                            </div>
                                        )
                                    }
                                    return (
                                        <div key={key} className={classes.msgDerecha} >
                                            <Paper className={classes.pDerecha}>
                                                <Typography variant="body1" component="p">
                                                    {item.mensaje}
                                                </Typography>
                                            </Paper>
                                        </div>
                                    )
                                })
                            }
                        </div>
                        <Divider />
                        <div className={classes.envio}>
                            <TextField
                                id="standard-multiline-flexible"
                                placeholder="Mensaje"
                                multiline
                                rowsMax={1}
                                value={text}
                                onChange={handleChange}
                                fullWidth
                            />
                            <Send color="primary" onClick={sendMessage}/>
                        </div>
                    </Paper>
                </Grow>
            )}
            </Popper>
            <QuestionAnswer
              ref={anchorRef}
              aria-haspopup="true"
              onClick={handleToggle}
              color="primary"
            />
        </div>
    );
}
