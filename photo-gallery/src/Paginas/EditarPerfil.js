import React from 'react';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import DialogoTomarFoto from './DialogoTomarFoto';
import Badge from '@material-ui/core/Badge';
import Avatar from '@material-ui/core/Avatar';
import Camera from '@material-ui/icons/CameraAlt';
import Divider from '@material-ui/core/Divider';
import Dialogo from './Dialogo';
import ElegirPhoto from './ElegirPhoto'
import ArrowBackIcon from '@material-ui/icons/ArrowBack';

import md5 from 'md5';
import '../Styles/Registro.css';
import ruta from './Ruta';

class EditarPerfil extends React.Component{
    constructor(props) {
      super(props);
      this.state = {
        mensajeError: '',
        copyPassword: '',
        error: true,
        registroDatos: {
          userName: '',
          name: '',
          password: '',
          namefoto: '',
          foto: '',
          changephoto: false
        },
        open: false,
        openConfirmacion: false
      }
      this.titulo = '';
      this.mensaje = '';
    }

    async componentDidMount() {
      await this.fetchConsulta();
    }
  
    fetchConsulta = async () => {
      try{
          let req = await fetch('http://' + ruta.ip + ':' + ruta.port + '/editar-perfil?idUser=' + localStorage.getItem('idUser'));
          let data = await req.json();
          if (data['status'] == 200){
            let registroDatos = this.state.registroDatos;
            registroDatos.userName = localStorage.getItem('user');
            registroDatos.name = data['name'];
            registroDatos.foto = 'https://practica1-g26-imagenes.s3.us-east-2.amazonaws.com/' + data['photo'];
            registroDatos.password = data['pass'];

            this.setState({
              registroDatos
            })
          }else if (data['status'] == 400){
            this.abrirConfirmacion('ERROR','Credenciales incorrectas!');
          }else{
            this.abrirConfirmacion('ERROR','Ocurrio un error! Intente de nuevo');
          }
      } catch (error) {
        this.abrirConfirmacion('ERROR','Ocurrio un error! Intente de nuevo');
      }
    }

    setCopyPass = async e  => {
      this.state.registroDatos.copyPassword = e.target.value;
      this.compararPass();
    }

    compararPass = () => {
      if(this.state.registroDatos.password !== md5(this.state.registroDatos.copyPassword)){
        this.setState({
          mensajeError: 'La contraseña no coincide',
          error: true
        });
        return true;
      }else{
        this.setState({
          mensajeError: '',
          error: false
        });
        return false;
      }
    }

    setUserName = async e  => {
      let registroDatos = this.state.registroDatos;
      registroDatos.userName = e.target.value;
      
      this.setState({
        registroDatos
      });
    }

    setName = async e  => {
      let registroDatos = this.state.registroDatos;
      registroDatos.name = e.target.value;
      
      this.setState({
        registroDatos
      });
    }

    handleCambiarFoto = async (namephoto, photo) => {
      let registroDatos = this.state.registroDatos;
      registroDatos.foto = photo;
      registroDatos.namefoto = namephoto
      registroDatos.changephoto = true

      this.setState({
        registroDatos
      });
    }

    handleEditar = async() => {
        //console.log(this.state.registroDatos);
        let config = {
          method: 'POST',
          headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Access-Control-Allow-Origin': '*',
              'Access-Control-Allow-Methods': 'POST, GET',
              'Access-Control-Request-Method': '*',
              'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
          },
          body: JSON.stringify({
              idUser: localStorage.getItem('idUser'),
              user: this.state.registroDatos.userName,
              name: this.state.registroDatos.name,
              changephoto: this.state.registroDatos.changephoto,
              namephoto: localStorage.getItem('user') + '-' + this.state.registroDatos.namefoto,
              photo: this.state.registroDatos.foto.replace(/^data:image\/\w+;base64,/, "")
          }),
          mode: 'cors'
        }
        
        try {
          let response = await fetch('http://' + ruta.ip + ':' + ruta.port + '/editar-perfil', config)
          let data = await response.json();
          
          if (data['status'] == 200){
            localStorage.setItem('user',this.state.registroDatos.userName);
            this.abrirConfirmacion('EDITAR PERFIL','Se edito el perfil correctamente');
          }else{
            this.abrirConfirmacion('ERROR','Ocurrio un error! Intente de nuevo');
          }
        } catch (error) {
          this.abrirConfirmacion('ERROR','Ocurrio un error! Intente de nuevo');
        }
        
    }

    abrirDialogo = () => {
      this.setState({
        open: true
      })
    }

    cerrarDialogo = () => {
      this.setState({
        open: false
      })
    }

    abrirConfirmacion = (titulo, mensaje) => {
      this.titulo = titulo;
      this.mensaje = mensaje;
      this.setState({
        openConfirmacion: true
      })
    }

    cerrarConfirmacion = () => {
      this.setState({
        openConfirmacion: false
      })
    }

    goInicio(){
      window.location.href = `/Inicio`;
    }

    render() {
        return (
          <Grid container className="root" spacing={2} justify="center">
            <Paper className="paper" elevation={5} justify="center">
              <h1>EDITAR PERFIL</h1>
              <Divider/>
              <Grid container spacing={2} style={{paddingBottom: "0em"}}>
                <Grid item xs={12} sm={7} style={{paddingLeft: "3em", paddingRight: "3em", paddingBottom: "0em"}}>
                  <TextField
                    label="Nombre de usuario" fullWidth
                    value={this.state.registroDatos.userName}
                    margin="normal" onChange={this.setUserName}
                  />
                  <TextField
                    label="Nombre" fullWidth
                    value={this.state.registroDatos.name}
                    margin="normal" onChange={this.setName}
                  />
                  <br/>
                  <br/>
                  <br/>
                  <br/>
                  <br/>
                  <br/>
                  <br/>
                  <TextField
                    label="Confirmar password" type="password"
                    helperText={this.state.mensajeError} error={this.state.error}
                    fullWidth margin="normal" onChange={this.setCopyPass}
                  />
                </Grid>
                <Grid item xs={12} sm={5} style={{paddingTop: "1em", paddingBottom: "0em"}}>
                  <Grid container spacing={2} justify="center">
                    <Badge
                      overlap="circle"
                      anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'right',
                      }}
                      badgeContent={
                        <Camera className="Icono"/>
                      }
                    >
                      <Avatar className="img" src={this.state.registroDatos.foto}
                        alt="Travis Howard" onClick={this.abrirDialogo}
                      />
                    </Badge> 
                    
                    <DialogoTomarFoto
                      handleCambiarFoto={this.handleCambiarFoto}
                      open = {this.state.open}
                      cerrarDialogo = {this.cerrarDialogo}
                    />
                  </Grid>
                  <Grid container className="root" spacing={2} justify="center">
                    <ElegirPhoto handleCambiarFoto={this.handleCambiarFoto}/>
                  </Grid>
                </Grid>
              </Grid>
              <Grid container spacing={2} justify="center">
                <Button variant="outlined" color="primary" disabled={this.state.error} onClick={this.handleEditar}>
                  editar
                </Button>
              </Grid>
              
              <Grid container className="root" spacing={2} justify="center" style={{paddingBottom: "1em"}}>
                <Typography >
                  <Link href="#" onClick={this.goInicio}>
                    <ArrowBackIcon/> Regresar
                  </Link>
                </Typography>
              </Grid>
            </Paper>
            <Dialogo 
              titulo = {this.titulo}
              mensaje = {this.mensaje}
              open = {this.state.openConfirmacion}
              cerrar = {this.cerrarConfirmacion}
            />
          </Grid>
        );
    }
}

export default EditarPerfil;