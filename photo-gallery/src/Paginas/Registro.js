import React from 'react';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import DialogoTomarFoto from './DialogoTomarFoto';
import Dialogo from './Dialogo';
import Badge from '@material-ui/core/Badge';
import Avatar from '@material-ui/core/Avatar';
import Camera from '@material-ui/icons/CameraAlt';
import Divider from '@material-ui/core/Divider';
import ElegirPhoto from './ElegirPhoto'

import '../Styles/Registro.css'
import ruta from './Ruta';

import md5 from 'md5';
import { v4 as uuidv4 } from 'uuid';

class Registro extends React.Component{

    state = {
      mensajeError: '',
      error: false,
      registroDatos: {
        userName: '',
        name: '',
        password: '',
        copyPassword: '',
        foto: '',
        namephoto: ''
      },
      open: false,
      openConfirmacion: false
    }

    goInicio = async() => {
      try {
        let config = {
          method: 'POST',
          headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Access-Control-Allow-Origin': '*',
              'Access-Control-Allow-Methods': 'POST, GET',
              'Access-Control-Request-Method': '*',
              'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
          },
          body: JSON.stringify({
              idUser: uuidv4(),
              user: this.state.registroDatos.userName,
              name: this.state.registroDatos.name,
              pass: md5(this.state.registroDatos.password),
              namephoto: this.state.registroDatos.userName + '-' + this.state.registroDatos.namephoto,
              photo: this.state.registroDatos.foto.replace(/^data:image\/\w+;base64,/, "")
          }),
          mode: 'cors'
        }
  
        let response = await fetch('http://' + ruta.ip + ':' + ruta.port + '/registro', config)
        let data = await response.json();
        console.log(data);
        if (data['status'] == 200){
          this.abrirConfirmacion('REGISTRO','Se registro exitosamente y se redirige a login');
          setTimeout(()=>{
            this.goLogin();
        }, 3000);
        }else if (data['status'] == 400){
          this.abrirConfirmacion('ERROR',`Ya existe un usuario con username ${this.state.registroDatos.userName}`);
        }else{
          this.abrirConfirmacion('ERROR','Ocurrio un error! Intente de nuevo');
        }  
      } catch (error) {
        this.abrirConfirmacion('ERROR','Ocurrio un error! Intente de nuevo');
      }
    }
    
    setUserName = async e  => {
      this.state.registroDatos.userName = e.target.value;
    }

    setName = async e  => {
      this.state.registroDatos.name = e.target.value;
    }

    setPass = async e  => {
      this.state.registroDatos.password = e.target.value;
      this.compararPass();
    }

    setCopyPass = async e  => {
      this.state.registroDatos.copyPassword = e.target.value;
      this.compararPass();
    }

    compararPass = () => {
      if(this.state.registroDatos.password !== this.state.registroDatos.copyPassword){
        this.setState({
          mensajeError: 'La contraseña no coincide',
          error: true
        });
      }else{
        this.setState({
          mensajeError: '',
          error: false
        });
      }
    }

    handleCambiarFoto = async (namephoto, photo) => {
      let registroDatos = this.state.registroDatos;
      registroDatos.foto = photo;
      registroDatos.namephoto = namephoto

      this.setState({
        registroDatos
      });
    }

    abrirDialogo = () => {
      this.setState({
        open: true
      })
    }

    cerrarDialogo = () => {
      this.setState({
        open: false
      })
    }

    abrirConfirmacion = (titulo, mensaje) => {
      this.titulo = titulo;
      this.mensaje = mensaje;
      this.setState({
        openConfirmacion: true
      })
    }

    cerrarConfirmacion = () => {
      this.setState({
        openConfirmacion: false
      })
    }

    goLogin(){
      window.location.href = `/`;
    }

    render() {
        return (
          <Grid container className="root" spacing={2} justify="center">
            <Paper className="paper" elevation={5} justify="center">
              <h1>REGISTRO</h1>
              <Divider/>
              <Grid container spacing={2} style={{paddingBottom: "0em"}}>
                <Grid item xs={12} sm={7} style={{paddingLeft: "3em", paddingRight: "3em", paddingBottom: "0em"}}>
                  <TextField
                    label="Nombre de usuario" fullWidth
                    margin="normal" onChange={this.setUserName}
                  />
                  <TextField
                    label="Nombre" fullWidth
                    margin="normal" onChange={this.setName}
                  />
                  <TextField
                    label="Password" type="password"
                    fullWidth margin="normal" onChange={this.setPass}
                  />
                  <TextField
                    label="Confirmar password" type="password"
                    helperText={this.state.mensajeError} error={this.state.error}
                    fullWidth margin="normal" onChange={this.setCopyPass}
                  />
                </Grid>
                <Grid item xs={12} sm={5} style={{paddingTop: "1em", paddingBottom: "0em"}}>
                  <Grid container  spacing={2} justify="center" style={{paddingTop: "2em"}}>
                    <Badge
                      overlap="circle"
                      anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'right',
                      }}
                      badgeContent={
                        <Camera className="Icono"/>
                      }
                    >
                      <Avatar className="img" src={this.state.registroDatos.foto}
                        alt="Travis Howard" onClick={this.abrirDialogo}
                      />
                    </Badge> 
                    
                    <DialogoTomarFoto
                      handleCambiarFoto={this.handleCambiarFoto}
                      open = {this.state.open}
                      cerrarDialogo = {this.cerrarDialogo}
                    />
                  </Grid>
                  <Grid container className="root" spacing={2} justify="center">
                    <ElegirPhoto handleCambiarFoto={this.handleCambiarFoto}/>
                  </Grid>
                </Grid>
              </Grid>
              <Grid container spacing={2} justify="center" >
                <Button variant="outlined" color="primary" onClick={this.goInicio}>
                  registrarse
                </Button>
              
              </Grid>
              <Grid container className="root" spacing={2} justify="center" style={{paddingBottom: "1em"}}>
                <Typography >
                  <Link href="#" onClick={this.goLogin}>
                    ¿Ya tienes cuenta? Logeate
                  </Link>
                </Typography>
              </Grid>
            </Paper>
            <Dialogo 
              titulo = {this.titulo}
              mensaje = {this.mensaje}
              open = {this.state.openConfirmacion}
              cerrar = {this.cerrarConfirmacion}
            />
          </Grid>
        );
    }
}

export default Registro;