import React from "react";
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import Avatar from '@material-ui/core/Avatar';
import Dialogo from './Dialogo';

import "../Styles/subirFoto.css";
import ruta from './Ruta';

class SubirFoto extends React.Component {
  state = {
    albumes: [],
    file: [],
    base64URL: "",
    album: "",
    namephoto: "",
    descripcion: "",
    openDialogo: false
  };

  getBase64 = file => {
    return new Promise(resolve => {
      let fileInfo;
      let baseURL = "";
      // Make new FileReader
      let reader = new FileReader();

      // Convert the file to base64 text
      reader.readAsDataURL(file);

      // on reader load somthing...
      reader.onload = () => {
        // Make a fileInfo Object
        //console.log("Called", reader);
        baseURL = reader.result;
        //console.log(baseURL);
        this.setState({imagen: baseURL});
        resolve(baseURL);
      };
      //console.log(fileInfo);
    });
  };

  handleFileInputChange = e => {
    //console.log(e.target.files[0]);
    let extension = e.target.files[0]['name'].split('.').pop().toLowerCase();
    if(extension == "png" || extension == "jpeg" || extension == "svg" || extension == "jpg"){
      let { file } = this.state;
      //console.log(e.target.files[0]['name']);
      file = e.target.files[0];

      this.getBase64(file)
        .then(result => {
          file["base64"] = result;
          //console.log("File Is", file);
          this.setState({
            namephoto: file['name'],
            base64URL: result,
            file
          });
        })
        .catch(err => {
          console.log(err);
        });

      this.setState({
        file: e.target.files[0]
      });
    }
  };

  handleChange = (event) => {
    this.setState({album:event.target.value});
    console.log('Album seleccionado: ' + this.state.album);
  };

  handleChangeDescription = (event) => {
    this.setState({descripcion:event.target.value});
  };

  async componentDidMount() {
    //await this.fetchConsulta();
  }

  fetchConsulta = async () => {
    try{
        let req = await fetch('http://' + ruta.ip + ':' + ruta.port + '/subir-foto?idUser=' + localStorage.getItem('idUser'));
        let data = await req.json();
        console.log(1, data)
        if (data['status'] == 200){
          this.setState({
            albumes: data['albumes']
          })
        }else if (data['status'] == 400){
          this.abrirDialogo('ERROR', 'Credenciales incorrectas!')
        }else{
          this.abrirDialogo('ERROR', 'Ocurrio un error! Intente de nuevo.')
        }
    } catch (error) {
      this.abrirDialogo('ERROR', 'Ocurrio un error! Intente de nuevo.')
    }
  }

  handleUpload = async() => {
    if (this.state.namephoto != '' && this.state.album != ('' || 'Ninguno')){
      let config = {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'POST, GET',
            'Access-Control-Request-Method': '*',
            'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
        },
        body: JSON.stringify({
            idUser: localStorage.getItem('idUser'),
            namephoto: localStorage.getItem('user') + '-' + this.state.namephoto,
            photo: this.state.base64URL.replace(/^data:image\/\w+;base64,/, ""),
            descripcion: this.state.descripcion
        }),
        mode: 'cors'
      }
      //console.log(config.body.photo);
      try {
        let response = await fetch('http://' + ruta.ip + ':' + ruta.port + '/subir-foto', config)
        let data = await response.json();
        console.log(data);
        if (data['status'] == 200){
          this.abrirDialogo('SUBIR FOTO', 'Se subio correctamente la foto');
        }else{
          this.abrirDialogo('ERROR', 'Ocurrio un error! Intente de nuevo')
        }
      } catch (error) {
        this.abrirDialogo('ERROR', 'Ocurrio un error! Intente de nuevo')
      }
    }
  }

  abrirDialogo = (titulo, mensaje) => {
    this.titulo = titulo;
    this.mensaje = mensaje;
    this.setState({
      openDialogo: true
    })
  }

  cerrarDialogo = () => {
    this.setState({
      openDialogo: false
    })
  }

  goInicio(){
    window.location.href = `/Inicio`;
  }

  render() {
    return (
      <div className="root">
      <Paper className="paper" elevation={5}>
        <div className="grid-container">
          <div className="grid-header">
            <h1>
              SUBIR FOTO
            </h1>
            <Divider/>
          </div>
          <div className="grid-body">
            <div className="row-1 column-1">
              <Avatar className="img" src={this.state.base64URL}
                        alt="Travis Howard"
                      />
            </div>
            <div className="row-1 column-2">
                <Box flexDirection="row-reverse" p={1} m={1} bgcolor="background.paper">    
                  <input type="file" id="fichero" onChange={this.handleFileInputChange} />
                  <label htmlFor="fichero" className="circle">Cargar Foto</label>
                </Box>
            </div>
            <div className="row-2">
                <Typography variant="body2" gutterBottom>
                  Nombre de la imagen
                </Typography>
                <Input placeholder={this.state.file['name']} readOnly={true} fullWidth inputProps={{ 'aria-label': 'description' }} />
            </div>
            <br/>
            <div className="row-3 column-1">
              <TextField
                id="standard-multiline-flexible"
                label="Descripcion"
                multiline
                rowsMax={4}
                value={this.state.descripcion}
                onChange={this.handleChangeDescription}
              />
                {
                /*
                <Typography variant="body2" gutterBottom>
                  Selecciona Album
                </Typography>
                <FormControl variant="outlined" className="formControl" fullWidth>
                  <InputLabel id="demo-simple-select-outlined-label">Album</InputLabel>
                  <Select
                    labelId="demo-simple-select-outlined-label"
                    id="demo-simple-select-outlined"
                    value={this.state.album}
                    onChange={this.handleChange}
                    label="Album"
                    fullWidth
                  >
                    <MenuItem key="Ninguno" value="Ninguno">
                    <em>None</em>
                    </MenuItem>
                    {
                      this.state.albumes.map((item) => {
                        return(
                          <MenuItem key={item.album} value={item.album}>
                            <em>
                              {item.album}
                            </em>
                          </MenuItem>
                        )
                      })
                    }
                  </Select>
                </FormControl>
                */
                }
            </div>
            <div className="row-3 column-2">
                <Box display="flex" flexDirection="row-reverse" p={1} m={1} bgcolor="background.paper">    
                  <Button variant="outlined" color="primary" onClick={this.handleUpload}>
                    Subir
                  </Button>
                </Box>
            </div>
          </div>
          <div className="grid-footer">      
            <Typography >
              <Link href="#" onClick={this.goInicio}>
                <ArrowBackIcon/>
                Regresar
              </Link>
            </Typography>
          </div>
        </div>
      </Paper>
      <Dialogo 
        titulo = {this.titulo}
        mensaje = {this.mensaje}
        open = {this.state.openDialogo}
        cerrar = {this.cerrarDialogo}
      />
    </div>
    );
  }
}

export default SubirFoto;
