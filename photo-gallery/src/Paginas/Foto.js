import React from 'react';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

import Detalle from './Detalle'
import '../Styles/Album.css';

class Foto extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            open: false
        }
    }

    abrirDetalle = () => {
        this.setState({
            open: true
        })
    }

    cerrarDetalle = () => {
        this.setState({
            open: false
        })
    }

    render() {
        let {image, item} = this.props;
        
        return (
            <>
                <Card
                    style={{ display: "flex", paddingBottom: "1em"}}
                    onClick={this.abrirDetalle}
                >
                    <CardMedia
                        style={{width: 150, height: 150}}
                        image={image}
                        title="Live from space album cover"
                    />
                    
                    <CardContent className="">
                        <Typography gutterBottom variant="h5" justify="center" component="h2">
                        {
                            item.foto == undefined ? item.S.replace('Fotos_Publicadas/','').replace('Fotos_Perfil/','').split('.')[0].replace(localStorage.getItem('user') + '-', '') : item.foto.replace('Fotos_Publicadas/','').replace('Fotos_Perfil/','').split('.')[0].replace(localStorage.getItem('user') + '-', '')
                        }
                        </Typography>
                    </CardContent>
                </Card>

                <Detalle
                    Nombre={item.foto == undefined ? item.S.replace('Fotos_Publicadas/','').replace('Fotos_Perfil/','').split('.')[0].replace(localStorage.getItem('user') + '-', '') : item.foto.replace('Fotos_Publicadas/','').replace('Fotos_Perfil/','').split('.')[0].replace(localStorage.getItem('user') + '-', '')}
                    Foto={image}
                    Descripcion={item.foto == undefined ? '' : item.descripcion}
                    open={this.state.open}
                    cerrar={this.cerrarDetalle}
                />
            </>
        );
    }
  }

  export default Foto;