import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Login from './Paginas/Login';
import Registro from './Paginas/Registro';
import Inicio from './Paginas/Inicio';
import EditarPerfil from './Paginas/EditarPerfil';
import SubirFoto from './Paginas/SubirFoto';
import EditarAlbumes from './Paginas/EditarAlbumes';
import VerFotos from './Paginas/VerFotos';
import ExtraerTexto from './Paginas/ExtraerTexto';

ReactDOM.render(
  
    <BrowserRouter>
      <Switch>
        <Route exact path = '/Login' component={Login} />
        <Route exact path = '/Registro' component={Registro} />
        <Route exact path = '/Inicio' component={Inicio} />
        <Route exact path = '/EditarPerfil' component={EditarPerfil} />
        <Route exact path = '/SubirFoto' component={SubirFoto} />
        <Route exact path = '/EditarAlbumes' component={EditarAlbumes} />
        <Route exact path = '/VerFotos' component={VerFotos} />
        <Route exact path = '/ExtraerTexto' component={ExtraerTexto} />
        <Route component={Login} />
      </Switch>
    </BrowserRouter>
  ,
  document.getElementById('root')
);
