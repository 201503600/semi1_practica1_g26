from flask import Flask, request, jsonify
from flask_cors import cross_origin

import boto3
from botocore.exceptions import ClientError
from boto3.dynamodb.conditions import Key

import base64
import uuid
import logging
import creds

# FORMA DE REALIZAR LAS CONEXIONES POR ARCHIVO COMPARTIDO
#s3_session = boto3.Session(profile_name='s3')
#ddb_session = boto3.Session(profile_name='dynamodb')
#s3_client = s3_session.resource('s3', region_name='us-west-2')
#ddb_client = ddb_session.resource('dynamodb', region_name='us-west-2')
# Variables globales
loggerErr = logging.getLogger('Error')
app = Flask(__name__)

@app.route('/', methods=["GET"])
@cross_origin()
def default():
    return 'Hello World!'

@app.route('/login', methods=["POST"])
@cross_origin()
def login():
    json = request.get_json(force=True)
    
    # Conexion a la base de datos
    dynamodb_client = boto3.client(
        'dynamodb',
        aws_access_key_id = creds.dynamodb['access_key_id'],
        aws_secret_access_key = creds.dynamodb['secret_access_key'],
        region_name = creds.dynamodb['region'],
    )

    try:
        # Se verifica que las credenciales coincidan
        response = dynamodb_client.scan(TableName = 'usuarios')

        for item in response['Items']:
            if item['user']['S'] == json['user'] and item['pass']['S'] == json['pass']:
                return jsonify({'status':200,'idUser':item['idUser']['S'], 'user':item['user']['S']})
    except ClientError as e:
        loggerErr.error(e)
        return jsonify({'status':500,'idUser':'','user':''})
    return jsonify({'status':400,'idUser':'','user':''})

# Metodo para validar el campo nombre de foto de la solicitud
def validarPhoto(idUser="",nombre="", user="", name="", password="", namephoto=""):
    if (nombre == ""):
        return {
            'idUser':{'S':idUser},
            'user':{'S':user},
            'name':{'S':name},
            'pass':{'S':password},
            'photo':{'S':''},
            'photos':{'L':[]}
        } 
    else:
        return {
            'idUser':{'S':idUser},
            'user':{'S':user},
            'name':{'S':name},
            'pass':{'S':password},
            'photo':{'S':namephoto},
            'photos':{'L':[{'S':namephoto}]}
        }

@app.route('/registro', methods=["POST"])
@cross_origin()
def registro():
    # Obteniendo parametros de la peticion
    json = request.get_json(force=True)
    namephoto = ""
    photo = ""

    # Decodificando la imagen
    if (json["photo"] != ""):
        namephoto = "Fotos_Perfil/" + json["namephoto"]
        try:
            photo = base64.b64decode(json["photo"])
        except:
            return jsonify({'status':500})
        try:
            s3_client = boto3.client(
                's3',
                aws_access_key_id = creds.s3['access_key_id'],
                aws_secret_access_key = creds.s3['secret_access_key'],
            )
            s3_client.put_object(Bucket="practica1-g26-imagenes",Key=namephoto,Body=photo,ContentType="image",ACL="public-read")
            #print(putresult) # Imprime el resultado de S3
        except ClientError as e:
            loggerErr.error(e)
            return jsonify({'status':500})

    # Conexion a la base de datos
    dynamodb_client = boto3.client(
        'dynamodb',
        aws_access_key_id = creds.dynamodb['access_key_id'],
        aws_secret_access_key = creds.dynamodb['secret_access_key'],
        region_name = creds.dynamodb['region'],
    )

    bandera = True
    
    try:
        # Se verifica que no exista un usuario con ese nombre
        response = dynamodb_client.scan(
            TableName = 'usuarios'
        )

        for item in response['Items']:
            if item['user']['S'] == json['user']:
                bandera = False
    except ClientError as e: 
        loggerErr.error(e)
        return jsonify({'status':500})

    # Si existe un item con el valor de la clave principal, no se registra el usuario
    if bandera:
        try:
            # Si no existe un item con el user especificado, se agrega a la DB
            item = validarPhoto(json['idUser'],json['namephoto'],json['user'],json['name'],json['pass'],namephoto)
            response = dynamodb_client.put_item(
                TableName='usuarios',
                Item=item
            )
        except ClientError as e: 
            logging.error(e)
            return jsonify({'status':500})
    else:
        return jsonify({'status':400})

    return jsonify({'status':200})

@app.route('/inicio', methods=["GET"])
@cross_origin()
def inicio():    
    # Conexion a la base de datos
    dynamodb_client = boto3.client(
        'dynamodb',
        aws_access_key_id = creds.dynamodb['access_key_id'],
        aws_secret_access_key = creds.dynamodb['secret_access_key'],
        region_name = creds.dynamodb['region'],
    )

    try:
        # Se obtiene nombre y photo de usuario con cierto ID
        response = dynamodb_client.get_item(
            TableName = 'usuarios',
            Key={
                'idUser':{'S': request.args.get('idUser')}
            },
            AttributesToGet = ['name', 'photo']
        )

        if 'Item' in response:
            return jsonify({
                            'status': 200,
                            'name':response['Item']['name']['S'],
                            'photo':response['Item']['photo']['S']
                        })

    except ClientError as e:
        loggerErr.error(e)
        return jsonify({'status':500,'name':'','photo':''})
    return jsonify({'status':400,'name':'','photo':''})

@app.route('/editar-perfil', methods=["GET", "POST"])
@cross_origin()
def editarPerfil():
    # Conexion a la base de datos
    dynamodb_client = boto3.client(
        'dynamodb',
        aws_access_key_id = creds.dynamodb['access_key_id'],
        aws_secret_access_key = creds.dynamodb['secret_access_key'],
        region_name = creds.dynamodb['region'],
    )

    if request.method == 'GET':
        try:
            # Se verifica que exista un usuario con ese nombre
            response = dynamodb_client.get_item(
                TableName = 'usuarios',
                Key={
                    'idUser':{'S': request.args.get('idUser')}
                },
                AttributesToGet = ['name', 'photo','pass']
            )

            if 'Item' in response:
                return jsonify({
                                'status':200,
                                'name':response['Item']['name']['S'],
                                'photo':response['Item']['photo']['S'],
                                'pass':response['Item']['pass']['S']
                            })

        except ClientError as e:
            loggerErr.error(e)
            return jsonify({'status':500,'name':'','photo':'','pass':''})
        return jsonify({'status':400,'name':'','photo':'','pass':''})
    else:
        json = request.get_json(force=True)
        idUser = json['idUser']
        user = json['user']  # Nuevo username
        name = json['name']
        namephoto = ""
        photo = ""

        if json['changephoto']:
            # Si hay cambio de foto de perfil se sube a S3
            namephoto = "Fotos_Perfil/" + json["namephoto"]
            photo = base64.b64decode(json["photo"])
            try:
                s3_client = boto3.client(
                    's3',
                    aws_access_key_id = creds.s3['access_key_id'],
                    aws_secret_access_key = creds.s3['secret_access_key'],
                )
                s3_client.put_object(Bucket="practica1-g26-imagenes",Key=namephoto,Body=photo,ContentType="image",ACL="public-read")
                #print(putresult) # Imprime el resultado de S3
            except ClientError as e:
                loggerErr.error(e)
                return jsonify({'status':500})

            # Luego se actualiza el item con la nueva foto
            try:
                # Se actualiza el campo photo de usuario
                response = dynamodb_client.get_item(
                    TableName = 'usuarios',
                    Key={
                        'idUser':{'S': idUser}
                    }
                )
                # Realizando los cambios
                response['Item']['photos']['L'].append({'S':namephoto})
                response['Item']['photo']['S'] = namephoto

                dynamodb_client.update_item(
                    TableName = 'usuarios',
                    Key = {'idUser':{'S':idUser}},
                    UpdateExpression = 'SET #P = :p, #PS = :ps',
                    ExpressionAttributeNames={
                        '#P': 'photo',
                        '#PS': 'photos',
                    },
                    ExpressionAttributeValues = {
                        ':p':{'S':namephoto},
                        ':ps':{'L':response['Item']['photos']['L']}
                    }
                )
            except ClientError as e:
                loggerErr.error(e)
                return jsonify({'status':500})
        
        # Si es diferente se actualiza la llave primaria
        try:
            dynamodb_client.update_item(
                TableName = 'usuarios',
                Key = {'idUser':{'S':idUser}},
                UpdateExpression = 'SET #U = :u, #N = :n',
                ExpressionAttributeNames={
                    '#U': 'user',
                    '#N': 'name',
                },
                ExpressionAttributeValues = {
                    ':u':{'S':user},
                    ':n':{'S':name}
                }
            )
        except ClientError as e:
            loggerErr.error(e)
            return jsonify({'status':500})
    return jsonify({'status':200})

@app.route('/subir-foto', methods=["GET", "POST"])
@cross_origin()
def subirFoto():
    # Conexion a la base de datos
    dynamodb_client = boto3.client(
        'dynamodb',
        aws_access_key_id = creds.dynamodb['access_key_id'],
        aws_secret_access_key = creds.dynamodb['secret_access_key'],
        region_name = creds.dynamodb['region'],
    )
    if request.method == 'GET':
        try:
            respuesta = []

            # Se verifica que no exista un usuario con ese nombre
            response = dynamodb_client.scan(TableName = 'albumes')

            for item in response['Items']:
                if item['idUser']['S'] == request.args.get('idUser'):
                    respuesta.append({'album':item['album']['S']})
            return jsonify({'status':200,'albumes':respuesta})
        except ClientError as e:
            loggerErr.error(e)
            return jsonify({'status':500,'albumes':[]})
    else:
        json = request.get_json(force=True)
        # La foto de album se sube a S3
        namephoto = "Fotos_Publicadas/" + json["namephoto"]
        photo = base64.b64decode(json["photo"])
        try:
            s3_client = boto3.client(
                's3',
                aws_access_key_id = creds.s3['access_key_id'],
                aws_secret_access_key = creds.s3['secret_access_key'],
            )
            s3_client.put_object(Bucket="practica1-g26-imagenes",Key=namephoto,Body=photo,ContentType="image",ACL="public-read")
            #print(putresult) # Imprime el resultado de S3
        except ClientError as e:
            loggerErr.error(e)
            return jsonify({'status':500})

        # Luego se actualiza el item con la nueva foto
        try:
            # Se obtiene el item del album que pertenece al usuario para agregar la foto
            response = dynamodb_client.get_item(
                TableName = 'albumes',
                Key={
                    'idUser':{'S': json['idUser']},
                    'album':{'S':json['album']}
                }
            )
            # Se agrega la foto
            response['Item']['photos']['L'].append({'S':namephoto})
            # Se actualiza el valor
            dynamodb_client.update_item(
                TableName = 'albumes',
                Key = {'idUser':{'S':json['idUser']},'album':{'S':json['album']}},
                UpdateExpression = 'SET #PS = :ps',
                ExpressionAttributeNames={
                    '#PS': 'photos',
                },
                ExpressionAttributeValues = {
                    ':ps':{'L':response['Item']['photos']['L']}
                }
            )
            return jsonify({'status':200})
        except ClientError as e:
            loggerErr.error(e)
            return jsonify({'status':500})

@app.route('/editar-albumes', methods=["GET", "POST"])
@cross_origin()
def editarAlbumes():
    # Conexion a la base de datos
    dynamodb_client = boto3.client(
        'dynamodb',
        aws_access_key_id = creds.dynamodb['access_key_id'],
        aws_secret_access_key = creds.dynamodb['secret_access_key'],
        region_name = creds.dynamodb['region'],
    )
    if request.method == 'GET':
        try:
            respuesta = []

            # Se verifica que no exista un usuario con ese nombre
            response = dynamodb_client.scan(TableName = 'albumes')

            for item in response['Items']:
                if item['idUser']['S'] == request.args.get('idUser'):
                    respuesta.append({'album':item['album']['S']})
            return jsonify({'status':200,'albumes':respuesta})

        except ClientError as e:
            loggerErr.error(e)
            return jsonify({'status':500,'albumes':[]})
    else:
        json = request.get_json(force=True)
        if json['operation'] == 'crear':
            try:
                dynamodb_client.put_item(
                    TableName='albumes',
                    Item={
                        'idUser':{'S':json['idUser']},
                        'album':{'S':json['album']},
                        'photos':{'L':[]}
                    }, # Agregar expected a servidor
                    Expected={
                        'album':{'Exists':False}
                    }
                )
            except ClientError as e:
                loggerErr.error(e)
                return jsonify({'status':500})
        else:
            try:
                dynamodb_client.delete_item(
                    TableName='albumes',
                    Key={
                        'idUser':{'S':json['idUser']},
                        'album':{'S':json['album']}
                    }
                )
            except ClientError as e:
                loggerErr.error(e)
                return jsonify({'status':500})
        return jsonify({'status':200})

@app.route('/ver-fotos', methods=["GET"])
@cross_origin()
def verFotos():
    # Conexion a la base de datos
    dynamodb_client = boto3.client(
        'dynamodb',
        aws_access_key_id = creds.dynamodb['access_key_id'],
        aws_secret_access_key = creds.dynamodb['secret_access_key'],
        region_name = creds.dynamodb['region'],
    )
    try:
        respuesta = {}
        response = dynamodb_client.get_item(
            TableName='usuarios',
            Key={
                'idUser':{'S':request.args.get('idUser')}
            },
            AttributesToGet = ['photo','photos']
        )
        if 'Item' in response:
            respuesta['photo'] = response['Item']['photo']['S']
            respuesta['photosProfile'] = response['Item']['photos']['L']
        
        # Se obtienen la lista de albumes
        respuesta['albumes'] = []
        response = dynamodb_client.scan(TableName='albumes')
        for item in response['Items']:
            if item['idUser']['S'] == request.args.get('idUser'):
                respuesta['albumes'].append({'album':item['album']['S'],'photos':item['photos']['L']})
        
        respuesta['status'] = 200
        return jsonify(respuesta)
    except ClientError as e:
        loggerErr.error(e)
        return jsonify({'status':500})
    return jsonify({'status':400})

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=4000, debug=True)