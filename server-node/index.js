process.env.PORT = process.env.PORT || 4000
process.env.HOST = process.env.HOST || '0.0.0.0'

//Rutas
const Acceso = require('./Rutas/Acceso');
const Perfil = require('./Rutas/Perfil');
const Albumes = require('./Rutas/Albumes');
const Fotos = require('./Rutas/Fotos');
const Texto = require('./Rutas/Texto');
const Chat = require('./Rutas/Chat');

const express = require('express');
const morgan = require('morgan');
const cors = require('cors');

var app = express();

app.use(express.urlencoded({ limit: '50mb', extended: false }));
app.use(express.json({limit: '50mb'}));
app.use(morgan('dev'));
app.use(cors());

//app.use(cookieParser());
//app.use(express.static(path.join(__dirname, 'public')));
app.get('/', function (req, res) {
    res.send('Bienvenido al servidor de NodeJS');
});

app.use('/', Acceso);
app.use('/', Perfil);
app.use('/', Albumes);
app.use('/', Fotos);
app.use('/', Texto);
app.use('/', Chat);

app.listen(process.env.PORT, process.env.HOST, ()=>{
    console.log(`Escuchando en http://${process.env.HOST}:${process.env.PORT}`)
})
