const AWS = require('aws-sdk');

/*const s3 = new AWS.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
});*/
const s3 = new AWS.S3({
    region: 'us-east-2',
    accessKeyId: 'AKIASDYXDQIOIGQJUBOS',
    secretAccessKey: 'F4/gbYJi00KXfKAfASvO7fO/Um3zAZjcpMQDPY21'
});

async function subirFoto(carpeta, namephoto, photo) {
    try {
        let decodedImage = Buffer.from(photo.replace(/^data:image\/\w+;base64,/, ""),'base64')

        const params = {
            Bucket: 'practica1-g26-imagenes',
            Key: `${carpeta}/${namephoto}`,
            Body: decodedImage,
            ContentType: "image",
            ACL: 'public-read'
        };
        await s3.putObject(params).promise();
        return true;

    } catch(error) {
        return false;
    }
}

async function getFoto(carpeta, photo) {
    const url_base = 'https://practica1-g26-imagenes.s3.us-east-2.amazonaws.com'
    return `${url_base}/${carpeta}/${photo}`;
}

async function getFotoRuta(ruta) {
    console.log(ruta)
    const url_base = 'https://practica1-g26-imagenes.s3.us-east-2.amazonaws.com'
    return `${url_base}/${ruta}`;
}

module.exports = {
    subirFoto: subirFoto,
    getFoto: getFoto,
    getFotoRuta: getFotoRuta
};