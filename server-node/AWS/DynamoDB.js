const AWS = require('aws-sdk');

const dynamo = new AWS.DynamoDB({
    apiVersion: '2012-08-10',
    region: 'us-east-2',
    accessKeyId: "AKIASDYXDQIONS7PFAW2",
    secretAccessKey: "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"
});


async function insertarElemento(params) {
    try {
        await dynamo.putItem(params).promise();
        return true;
    } catch (error) {
        return false;
    }
}

async function updateElemento(params) {
    try {
        await dynamo.updateItem(params).promise();
        return true;
    } catch (error) {
        return false;
    }
}

async function getElemento(params) {
    try {
        return await dynamo.getItem(params).promise();
    } catch (error) {
        return 500;
    }
}

async function existElemento(nameTable, user) {
    try {
        let data = await getAll(nameTable);
        
        if(typeof data === 'object'){
            for (const json of data) {
                console.log(json.user.S, user)
                if(json.user.S === user){
                    console.log(json.user.S, user)
                    return 200;
                }
            }
            
            return 400;
        }
        return 500;
    } catch (error) {
        return 500;
    }
}

async function getAll(nameTable) {
    try {
        return await (await dynamo.scan({TableName: nameTable}).promise()).Items;
    } catch (error) {
        return 500
    }
}

async function deleteElemento(params) {
    try {
        return await dynamo.deleteItem(params).promise();
    } catch (error) {
        return 500
    }
}

module.exports = {
    insertarElemento: insertarElemento,
    existElemento: existElemento,
    getElemento: getElemento,
    getAll: getAll,
    updateElemento: updateElemento,
    deleteElemento: deleteElemento    
};
