var express = require('express');
var router = express.Router();

const S3 = require('../AWS/S3');
const DynamoDB = require('../AWS/DynamoDB');

router.get('/editar-perfil', async function(req, res, next) {
    try {
        const idUser = req.query.idUser;

        if(idUser !== undefined) {
            const params = {
                TableName: 'usuarios',
                Key: {
                    'idUser': {S: idUser}
                },
                AttributesToGet: ['name', 'photo', 'pass']
            };

            let data = await DynamoDB.getElemento(params);

            if(typeof data === 'object'){
                data = data.Item;
                if(data !== undefined){
                    return res.json({
                        status: 200,
                        name: data.name.S,
                        photo: data.photo.S,
                        pass: data.pass.S
                    });
                }
                return res.json({status: 400});
            }
        }
        return res.json({status: 500});
    } catch (error) {
        return res.json({status: 500});
    }
});

router.post('/editar-perfil', async function(req, res, next) {
    try {
        const idUser = req.body.idUser;
        let namephoto = req.body.namephoto;
        let photo = req.body.photo;
        let params = {}

        if(req.body.changephoto === true){
            if(!await S3.subirFoto('Fotos_Perfil', namephoto, photo)){
                return res.json({status: 400});
            }
            photo = `Fotos_Perfil/${namephoto}`
            namephoto = photo

            params = {
                TableName: 'usuarios',
                Key: {
                    'idUser': {S: idUser}
                },
                AttributesToGet: ['photos']
            };
            let data = await DynamoDB.getElemento(params);
            if(typeof data !== 'object'){
                return res.json({status: 400});
            }

            data = data.Item.photosprofile.L;
            if(req.body.changephoto === true){
                data.push({S: photo})
            }
            photo = data
            
            params = {
                TableName: 'usuarios',
                Key: {
                    'idUser': {S: idUser}
                },
                UpdateExpression: 'SET #U = :u, #N = :n, #P = :p, #PS = :ps',
                ExpressionAttributeNames: {
                    '#U': 'user',
                    '#N': 'name',
                    '#P': 'photo',
                    '#PS': 'photosprofile'
                },
                ExpressionAttributeValues: {
                    ':u': {'S': req.body.user},
                    ':n': {'S': req.body.name},
                    ':p': {'S': namephoto},
                    ':ps': {'L': photo}
                }
            };
        }else{
            params = {
                TableName: 'usuarios',
                Key: {
                    'idUser': {S: idUser}
                },
                UpdateExpression: 'SET #U = :u, #N = :n',
                ExpressionAttributeNames: {
                    '#U': 'user',
                    '#N': 'name'
                },
                ExpressionAttributeValues: {
                    ':u': {'S': req.body.user},
                    ':n': {'S': req.body.name}
                }
            };
        }
        
        if(await DynamoDB.updateElemento(params)){
            return res.json({
                status: 200,
                user: req.body.user
            });
        }
        return res.json({status: 400});
    } catch(errror) {
        return res.json({status: 500});
    }
});

module.exports = router;