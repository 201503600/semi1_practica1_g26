var express = require('express');
const AWS = require('aws-sdk');
var router = express.Router();

const Translate = new AWS.Translate({
    region: 'us-east-2',
    accessKeyId: 'AKIASDYXDQIOIDZUCM4I',
    secretAccessKey: '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'
});

const Rekognition = new AWS.Rekognition({
    region: 'us-east-2',
    accessKeyId: 'AKIASDYXDQIOL5I2K2HV',
    secretAccessKey: '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'
});

router.post('/traducir', async function(req, res, next) {
    const texto = req.body.texto;
    let idioma = req.body.idioma;

    switch(idioma){
        case 'Inglés':
            idioma = 'en';
            break;
        case 'Portugués':
            idioma = 'pt'
            break;
        case 'Francés':
            idioma = 'fr'
            break;
        case 'Sueco':
            idioma = 'sv'
            break;
        default:
            idioma = 'es'
    }

    const params = {
        "Text": texto,
        "SourceLanguageCode": 'auto',
        "TargetLanguageCode": idioma
    };

    Translate.translateText(params, function (err, data) {
        if (err) {
            return res.json({status: 400});
        } else {
            console.log(data);
            return res.json({status: 200, data});
        }
    });
});

router.post('/extraer-texto', async function(req, res, next) {
    const Imagen = req.body.Imagen;    
    const params = {
        Image: {
            Bytes: Buffer.from(Imagen.replace(/^data:image\/\w+;base64,/, ""),'base64')
        }
    };

    Rekognition.detectText(params, function (err, data) {
        if (err) {
            return res.json({status: 400});
        } else {
            
            let text = '';
            let anterior;

            for(const item of data.TextDetections){
                if(item.Type === 'WORD'){
                    if(anterior !== undefined){
                        if(Math.abs(item.Geometry.BoundingBox.Top-anterior) > 0.02){
                            text += String.fromCharCode(10);
                        }
                    }
                    anterior = item.Geometry.BoundingBox.Top;
                    text += `${item.DetectedText} `;
                }
                }
            return res.json({status: 200, text});
        }
    });
});

module.exports = router;