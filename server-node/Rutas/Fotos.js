var express = require('express');
var router = express.Router();

const DynamoDB = require('../AWS/DynamoDB');
const S3 = require('../AWS/S3');
var AWS = require('aws-sdk');

var {encode, decode} = require( 'node-base64-image');
const Rekognition = new AWS.Rekognition({
    region: 'us-east-2',
    accessKeyId: 'AKIASDYXDQIOL5I2K2HV',
    secretAccessKey: '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'
});

/*router.get('/subir-foto', async function(req, res, next) {
    try {
        const idUser = req.query.idUser;
        let data = await DynamoDB.getAll('albumes');
        
        if(data !== 500){
            let albumes = []

            for (const json of data) {
                if(json.idUser.S === idUser){
                    albumes.push({album: json.album.S})
                }
            }    
            return res.json({status: 200, albumes: albumes});
        }
        return res.json({status: 400});
    } catch (error) {
        return res.json({status: 500});    
    }
});
*/
router.post('/subir-foto', async function(req, res, next) {
    try {
        const idUser = req.body.idUser;
        const descripcion = req.body.descripcion;
        const namephoto = req.body.namephoto;
        const photo = req.body.photo;

        if(await S3.subirFoto('Fotos_Publicadas', namephoto, photo)){
            const params = {
                TableName: 'usuarios',
                Key: {
                    'idUser': {S: idUser}
                },
                AttributesToGet: ['photos']
            };
            let data = await DynamoDB.getElemento(params);
            
            if(typeof data === 'object'){
                data = data.Item;
                if(data !== undefined){
                    let photos = data.photos.L;
                    photos.push({L:[{S: `Fotos_Publicadas/${namephoto}`},{S:`${descripcion}`}]})

                    let params = {
                        TableName: 'usuarios',
                        Key: {
                            'idUser': {S: idUser}
                        },
                        UpdateExpression: 'SET #PS = :ps',
                        ExpressionAttributeNames: {'#PS': 'photos'},
                        ExpressionAttributeValues: {':ps': {'L': photos}}
                    };
                    
                    if(await DynamoDB.updateElemento(params)){
                        return res.json({status: 200});
                    }
                }
            }
        }
        return res.json({status: 400});    
    } catch (error) {
        return res.json({status: 500});
    }
});

router.get('/ver-fotos', async function(req, res, next) {
    try {
        const idUser = req.query.idUser;
        const options = {
            string: true,
            headers: {
              "User-Agent": "Ugram-pro"
            }
        };
        
        /*let data = await DynamoDB.getAll('albumes');
        
        if(typeof data === 'object'){
            /*let albumes = []

            for (const json of data) {
                if(json.idUser.S === idUser){
                    albumes.push({
                        album: json.album.S,
                        photos: json.photos.L
                    })
                }
            }*/
            
            const params = {
                TableName: 'usuarios',
                Key: {
                    'idUser': {S: idUser}
                },
                AttributesToGet: ['photo', 'photos', 'photosProfile']
            };
            //console.log(params)

            data = await DynamoDB.getElemento(params);

            if(typeof data === 'object'){
                data = data.Item;
                if(data !== undefined){
                    const seleccionPhotos = new Map();
                    for(const photo of data.photos.L){
                        const imagen = await encode('https://practica1-g26-imagenes.s3.us-east-2.amazonaws.com/' + photo.L[0].S, options);
                        const params = {
                            Image: { 
                              Bytes: Buffer.from(imagen, 'base64')
                            }, 
                            MaxLabels: 123
                        };
                        let etiquetas = await Rekognition.detectLabels(params).promise();
                        //console.log("Primer respuesta ", etiquetas);
                        for (const etiqueta of etiquetas.Labels){
                            if (etiqueta.Confidence >= 85){
                                //console.log(etiqueta);
                                if (seleccionPhotos.has(etiqueta.Name)){
                                    let backup = seleccionPhotos.get(etiqueta.Name);
                                    backup.push({"foto":photo.L[0].S, 
                                                    "descripcion":photo.L[1].S});
                                    seleccionPhotos.set(etiqueta.Name, Array.from(backup));
                                } else {
                                    seleccionPhotos.set(etiqueta.Name, [{"foto":photo.L[0].S, 
                                                                            "descripcion":photo.L[1].S}]);
                                }                            
                            }
                        }
                        console.log(photo.L[0].S);
                        console.log(photo.L[1].S);
                    }
                    console.log(seleccionPhotos);
                    return res.json({
                        status: 200,
                        photo: data.photo.S,
                        photos: Array.from(seleccionPhotos),
                        photosProfile: data.photosProfile.L
                    });
                }
            }
            return res.json({status: 400});
        /*}
        return res.json({status: 400});*/
    } catch (error) {
        return res.json({status: 500});    
    }
});

module.exports = router;