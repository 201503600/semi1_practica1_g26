var express = require('express');
var router = express.Router();

const DynamoDB = require('../AWS/DynamoDB');

router.get('/editar-albumes', async function(req, res, next) {
    try {
        const idUser = req.query.idUser;
        let data = await DynamoDB.getAll('albumes');
        
        if(data !== 500){
            let albumes = []

            for (const json of data) {
                if(json.idUser.S === idUser){
                    albumes.push({album: json.album.S})
                }
            }    
            return res.json({status: 200, albumes: albumes});
        }
        return res.json({status: 400});
    } catch (error) {
        return res.json({status: 500});    
    }
});

router.post('/editar-albumes', async function(req, res, next) {
    try {
        const operation = req.body.operation;
        
        if(operation === 'crear'){
            const params = {
                TableName: 'albumes',
                Item: {
                    'idUser': {S: req.body.idUser},
                    'album': {S: req.body.album},
                    'photos': {L: []}
                },
                "Expected" : {"album":{"Exists":false}}
            };
            
            if(await DynamoDB.insertarElemento(params)) {
                return res.json({status: 200});
            }
        }else{   
            const params = {
                TableName: 'albumes',
                Key: {
                    'idUser': {S: req.body.idUser},
                    'album': {S: req.body.album}
                }
            };
            
            if(await DynamoDB.deleteElemento(params)) {
                return res.json({status: 200});
            }
        }
        return res.json({status: 400});
    } catch (error) {
        return res.json({status: 500});
    }
});

module.exports = router;