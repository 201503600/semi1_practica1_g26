const uuidv4 = require("uuid/v4");
var express = require('express');
var router = express.Router();

const AWS = require('aws-sdk');
const S3 = require('../AWS/S3');
const DynamoDB = require('../AWS/DynamoDB');

var {encode, decode} = require( 'node-base64-image');
const Rekognition = new AWS.Rekognition({
    region: 'us-east-2',
    accessKeyId: 'AKIASDYXDQIOL5I2K2HV',
    secretAccessKey: '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'
});

router.post('/login', async function(req, res, next) {
    try {
        let data = await DynamoDB.getAll('usuarios');
        const user = req.body.user;
        const pass = req.body.pass;

        for (const json of data) {
            if(json.user.S === user && json.pass.S === pass){
                return res.json({
                    status: 200,
                    idUser: json.idUser.S,
                    user: user
                });
            }
        }
        res.json({status: 400, idUser: '', user: ''});
    } catch (error) {
        res.json({status: 500, idUser: '', user: ''});
    }
});

router.post('/loginCamera', async function(req, res, next){
    try {
        let data = await DynamoDB.getAll('usuarios');
        const user = req.body.user;
        const camera = req.body.camera;
        const options = {
            string: true,
            headers: {
              "User-Agent": "Ugram-pro"
            }
        };

        for (const json of data) {
            if(json.user.S === user){
                const photo = await encode('https://practica1-g26-imagenes.s3.us-east-2.amazonaws.com/' + json.photo.S, options);
                var params = {    
                    SourceImage: {
                        Bytes: Buffer.from(photo, 'base64')     
                    }, 
                    TargetImage: {
                        Bytes: Buffer.from(camera, 'base64')    
                    },
                    SimilarityThreshold: '85'                   
                };
                Rekognition.compareFaces(params, function(err, data) {
                    if (err) {
                        console.log(err);
                        res.json({status: 400, idUser: '', user: ''});
                    } else {                           
                        res.json({
                            status: 200,
                            idUser: json.idUser.S,
                            user: user
                        });      
                    }
                });
            }
        }
    } catch (error) {
        res.json({status: 500, idUser: '', user: ''});
    }
});

router.post('/registro', async function(req, res, next) {
    try {
        const user = req.body.user;
        
        if(await DynamoDB.existElemento('usuarios', user) === 400) {
            const carpeta = 'Fotos_Perfil';
            const photo = req.body.photo;
            const namephoto = req.body.namephoto;
            let item = {}
            
            if(namephoto !== ''){
                if(!await S3.subirFoto(carpeta, namephoto, photo)){
                    return res.json({status: 400});
                }
                
                item = {
                    'idUser': {S: uuidv4()},
                    'user': {S: user},
                    'name': {S: req.body.name},
                    'pass': {S: req.body.pass},
                    'photo': {S: `${carpeta}/${namephoto}`},
                    'photos': {L: []},
                    'photosProfile': {L: [{S: `${carpeta}/${namephoto}`}]}
                }
            }else{
                item = {
                    'idUser': {S: uuidv4()},
                    'user': {S: user},
                    'name': {S: req.body.name},
                    'pass': {S: req.body.pass},
                    'photo': {S: namephoto},
                    'photos': {L: []},
                    'photosProfile': {L: []}
                }
            }

            params = {
                TableName: 'usuarios',
                Item: item
            };
        
            if(await DynamoDB.insertarElemento(params)) {
                return res.json({status: 200});
            }
            return res.json({status: 500});
        }
        return res.json({status: 400});
    } catch (error) {
        return res.json({status: 500});
    }
    
});

router.get('/inicio', async function(req, res, next) {
    try {
        const idUser = req.query.idUser;
        const options = {
            string: true,
            headers: {
              "User-Agent": "Ugram-pro"
            }
        };
        const etiquetas = [];
        if(idUser !== undefined) {
            const params = {
                TableName: 'usuarios',
                Key: {
                    'idUser': {S: idUser}
                },
                AttributesToGet: ['user', 'name', 'photo']
            };

            let data = await DynamoDB.getElemento(params);

            if(typeof data === 'object'){
                data = data.Item;
                if(data !== undefined){
                    const photo = await encode('https://practica1-g26-imagenes.s3.us-east-2.amazonaws.com/' + data.photo.S, options);
                    const params = {
                        Image: { 
                          Bytes: Buffer.from(photo, 'base64')
                        }, 
                        Attributes: ['ALL']
                    };
                    await Rekognition.detectFaces(params, function(err, data2) {
                        if (err) {
                            console.log(err);
                            res.json({
                                status: 500
                            });
                        } else { 
                            //console.log(data2);  
                            
                            for (const etiqueta of data2.FaceDetails){
                                etiquetas.push(etiqueta.Gender.Value);
                                etiquetas.push(etiqueta.AgeRange.Low + ' - ' + etiqueta.AgeRange.High + ' años')
                                if (etiqueta.Smile.Value)
                                    etiquetas.push('Sonrisa')
                                if (etiqueta.Sunglasses.Value)
                                    etiquetas.push('Lentes de sol')
                                if (etiqueta.Beard.Value)
                                    etiquetas.push('Barba')
                                if (etiqueta.EyesOpen.Value)
                                    etiquetas.push('Ojos abiertos')
                                if (etiqueta.MouthOpen.Value)
                                    etiquetas.push('Boca abierta')
                                for(const emocion of etiqueta.Emotions){
                                    if (emocion.Confidence >= 80)
                                        etiquetas.push(emocion.Type);
                                }

                            }
                            
                            res.json({
                                status: 200,
                                user: data.user.S,
                                name: data.name.S,
                                photo: data.photo.S,
                                labels: etiquetas
                            });
                        }
                    });
                }
            }
        }
    } catch (error) {
        return res.json({status: 500});
    }
});

module.exports = router;