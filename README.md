# PRACTICA 1
## INTEGRANTES
---
> **Carnet:** 201603102  
> **Nombre:** Willians Alberto Lemus López  

> **Carnet:** 201503600  
> **Nombre:** Edgar Daniel Cil Peñate  

## ARQUITECTURA
---
Para la practica se hizo uso de varios servicios de AWS; se cuenta con:

- Una pagina web almacenada en un bucket de S3 (llamado practica1-g26-paginaweb), dicha página sirve para interactuar con el sistema.

- Un servidor API Rest (NodeJS) el cual esta almacenado en una instancias de EC2 y con politicas establecidas por un Security Group, este servidor se comunica con un Bucket de imagenes y la base de datos.

- Comunicación del servidor con Rekognition para el analisis de imagenes, las cueles involucran las fotos de perfil y las imagenes que se almaceran en albumes de manera automática.

- Comunicación del servidor con Lex, el cual provee diversas funciones de chatbot con las cuales el usuario podra interactuar.

- Un Bucket de S3 (llamado practica1-g26-imagenes) donde se almacenan las fotos de perfil (en la capeta Fotos_Perfil) y las fotos subidas por los usuarios a sus albumes (en la carpeta Fotos_Publicadas)

- Una base de datos del sercico DynamoDB que sirve para llevar el control de los usuarios, albumes y fotos.  

    ![Arquitectura](./Capturas/Arquitectura.png)

## USUARIO IAM
---
> ### **Servicio S3**
- **201603102_s3:**  Usuario creado para poder hacer uso del servicio S3 desde el servidor de NodeJS. Con este usuario se puede almacenar las imagenes que se suben desde la pagina web. Se le asignó la politica de AmazonS3FullAccess.  
  
> ### **Servicio DynamoDB**
- **201603102_dynamodb:**  Usuario creado para poder hacer uso del servicio DynamoDB desde el servidor de NodeJS, para poder realizar las inserciones y recuperaciones de los datos necesarias para el correcto funcionamiento del sistema. Se le asignó la politica de AmazonDynamoDBFullAccess.  

> ### **Servicio Rekognition**  
- **rekognition:**  Usuario creado para poder hacer uso del servicio Rekognition desde el servidor de NodeJS, para la extracción del texto, recuperacion de etiquetas e inicio de sesion por de medio de camara. Se le asignó la politica de AmazonRekognitionFullAccess.  

> ### **Servicio Translate**  
- **translate:**  Usuario creado para poder hacer uso del servicio translate desde el servidor de NodeJS, para la traducción de las descripciones de las fotos, está traducción puede ser en diversos idiomas. Se le asignó la politica de TranslateFullAccess.  

> ### **Servicio Lex**  
- **lex:**  Usuario creado para poder hacer uso del servicio lex desde el servidor de NodeJS, para que sirva de intermediario entre pagina web y el chatbot creado. Se le asignó la politica de AmazonLexFullAccess.  

> ### **Servicio EC2**  
- **Administrador_201603102:**  Usuario creado para poder hacer uso del servicio EC2. Se le asignó la politica de AdministradorAccess.  

- **Administrador_201503600:**  Usuario creado para poder hacer uso del servicio EC2. Se le asignó la politica de AdministradorAccess.  

## FUNCIONALIDADES DEL CHATBOT
> ### **Ayuda**  
Muestra la lista de entradas que se puede utilizar en el chatbot para poder empezar la interacción con el mismo.

> ### **Calificación**  
Permite que el usuario de una calificación a la aplicacion tanto en su utilidad, amigabilidad con el usuario, etc.

> ### **Editar perfil**  
Permite que el usuario pueda modificar su username y nombre.

> ### **Listar emociones**  
Lista las emociones detectadas en la foto de perfil con un porcentaje de confianza mayor o igual al 80%.

> ### **Traducir texto**  
Permite que el usuario pueda traducir un texto escrito al idioma que eligio entre los que se encuentran disponibles.

## FUNCIONES IMPLEMENTADAS CON REKOGNITION
> ### **Extraer texto**  
Función que se utiliza para traducir las descripciones de las fotos y tambien texto introducido en chatbot cuando se haga uso de esa funcionalidad.

> ### **Comparar rostros**  
Función utilizada para comparar la entrada de la camara con las fotos de perfil con el objetivo de que se pueda iniciar sesión en el sistema por medio de la camara web.

> ### **Detección de etiquetas**  
Función que sirve para obtener las etiquetas que devulve rekognition tanto para mostrar que tiene un porcentaje de confianza mayor o igual al 80% y para la asignación automática de albumes.

> ### **Face analytics**  
Función que sirve para obtener la lista de emociones de la foto de perfil atravez del chatbot, se muestran solo las emociones que cumplan con un porcentaje de confianza mayor o igual al 80%.
